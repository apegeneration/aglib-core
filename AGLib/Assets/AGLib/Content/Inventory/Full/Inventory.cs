﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MessagePack;
using UnityEngine;

namespace AGLib.Content.Inventory.Full
{
    [Serializable]
    [MessagePackObject(true)]
    public class Inventory
    {
        // Inventory ID
        public int id;

        // Content
        public List<Item> content;

        // Inventory max size
        private int size;


        public Inventory() { }

        public Inventory(int size, Inventory save = null)
        {
            this.size = size;

            if (save == null)
                Initialize();
            else
                Initialize(save);
        }

        public Inventory(int id, int size, Inventory save = null)
        {
            this.id = id;
            this.size = size;

            if (save == null)
                Initialize();
            else
                Initialize(save);
        }

        public bool AddItem(Item item)
        {
            return AddItem(item.id, item.amount);
        }

        public bool AddItem(int id, int amount)
        {
            var index = content.FindIndex(x => x.id == id);
            if (index == -1)
                return false;

            content[index].amount += amount;

            return true;
        }

        public bool AddItem(int id, int amount, int max)
        {
            var index = content.FindIndex(x => x.id == id);
            if (index == -1)
                return false;

            var a = content[index].amount + amount;
            if (a <= max)
                content[index].amount += amount;
            else
            {
                var d = a - max;
                content[index].amount = max;
                var i = content[index].Clone();
                i.amount = d;
            }

            return true;
        }

        public bool AddItem(int id, bool stackable)
        {
            if (stackable)
            {
                var index = content.FindIndex(x => x.id == id);
                if (index == -1)
                    return false;

                content[index].amount += 1;

                return true;
            }

            if (content.Count >= size)
                return false;

            content.Add(new Item(id));

            return true;
        }

        public Item RemoveItemByIndex(int slotindex, int amount = 1, bool all = false)
        {
            if (slotindex < 0 || slotindex > size)
                return null;

            if (all)
            {
                var i = content[slotindex].Clone();
                content.RemoveAt(slotindex);

                return i;
            }

            content[slotindex].amount -= amount;
            if (content[slotindex].amount < 0)
            {
                var i = content[slotindex].Clone();
                content.RemoveAt(slotindex);

                return i;
            }

            return null;
        }

        public Item RemoveItemById(int id, int amount = 1, bool all = false)
        {
            var index = content.FindIndex(x => x.id == id);
            if (index == -1)
                return null;

            if (all)
            {
                var i = content[index].Clone();
                content.RemoveAt(index);

                return i;

            }

            content[index].amount -= amount;
            if (content[index].amount < 0)
            {
                var i = content[index].Clone();
                content.RemoveAt(index);

                return i;
            }

            return new Item(content[index].id, amount);
        }

        public void SetSize(int size)
        {
            this.size = size;
        }

        public int GetSize()
        {
            return content.Count;
        }

        public int GetFreeSlots()
        {
            return size - content.Count;
        }

        private void Initialize(Inventory save = null)
        {
            if (save != null)
            {
                id = save.id;
                
                content = save.content?.ConvertAll(
                    x => x.Clone()) ?? new List<Item>();
            }
            else
                content = new List<Item>();
        }
    }
}
