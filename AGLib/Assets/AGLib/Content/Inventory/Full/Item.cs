﻿using System;
using System.Collections;
using System.Collections.Generic;
using AGLib.Interfaces;
using MessagePack;
using UnityEngine;

namespace AGLib.Content.Inventory.Full
{
    [Serializable]
    [MessagePackObject(true)]
    public class Item : ICloneable<Item>
    {
        // Name *
        public string name;

        // Identifier *
        public int id;

        // Amount of the item
        public int amount;


        public Item() { }

        public Item(int id)
        {
            this.id = id;
            amount = 1;
        }

        public Item(int id, int amount)
        {
            this.id = id;
            this.amount = amount;
        }

        public Item Clone()
        {
            var i = new Item
            {
                id = id,
                amount = amount
            };

            return i;
        }
    }
}