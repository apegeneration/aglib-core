﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGLib.Utilities
{
    [Serializable]
    public class NamePack
    {
        public string name;

        public List<string> female;
        public List<string> male;
        public List<string> surname;
        public List<string> nickname;


        public NamePack()
        {
        }

        public NamePack(string name)
        {
            this.name = name;
        }

        public virtual void Initialize()
        {
            female = new List<string>();
            male = new List<string>();
            surname = new List<string>();
            nickname = new List<string>();
        }

        public string GetMale(int index)
        {
            return index < 0 || index >= male.Count
                ? male[0]
                : male[index];
        }

        public string GetFemale(int index)
        {
            return index < 0 || index >= female.Count
                ? female[0]
                : female[index];
        }

        public string GetSurname(int index)
        {
            return index < 0 || index >= surname.Count
                ? surname[0]
                : surname[index];
        }

        public string GetNickname(int index)
        {
            return index < 0 || index >= nickname.Count
                ? nickname[0]
                : nickname[index];
        }

        public int GetCountFemale()
        {
            return female.Count;
        }

        public int GetCountMale()
        {
            return male.Count;
        }

        public int GetCountSurname()
        {
            return surname.Count;
        }

        public int GetCountNickname()
        {
            return nickname.Count;
        }
    }
}
