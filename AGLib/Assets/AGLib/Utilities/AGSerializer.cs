﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MessagePack;

namespace AGLib.Utilities
{
    /// <summary>
    /// Serializer
    /// </summary>
    public static class AGSerializer
    {
        /// <summary>
        /// Serialize object using Binary Formatter
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="target">Target to serialize</param>
        /// <returns>Serialized bytes</returns>
        public static byte[] SerializeWithBinaryFormatter<T>(T target)
        {
            // Create binary formatter
            var bf = new BinaryFormatter();

            // Serialize
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, target);

                return ms.ToArray();
            }
        }

        /// <summary>
        /// Deserialize object using Binary Formatter
        /// </summary>
        /// <param name="target">Target to deserialize</param>
        /// <returns>Deserialized object</returns>
        public static object DeserializeWithBinaryFormatter(byte[] target)
        {
            var bf = new BinaryFormatter();

            using (var ms = new MemoryStream(target))
                return bf.Deserialize(ms);
        }

        /// <summary>
        /// Serialize T using MessagePack
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="target">Target object</param>
        /// <returns>Serialized bytes</returns>
        public static byte[] SerializeWithMessagePack<T>(T target)
        {
            return MessagePackSerializer.Serialize(target);
        }
    }
}
