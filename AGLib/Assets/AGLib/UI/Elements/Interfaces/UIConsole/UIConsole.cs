﻿using System.Collections;
using System.Collections.Generic;
using AGLib.UI.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace AGLib.UI.Interface
{
    public class UIConsole : UIInterface
    {
        [Tooltip("True: Remember, False: Reset")]
        public bool unloadUserInput;

        [Tooltip("If user is typing (auto)")]
        public bool isTyping;

        [Tooltip("Console output text")]
        public Text textOutput;

        [Tooltip("Vertical scroll bar")]
        public Scrollbar scrollerVertical;

        [Tooltip("Console user input field")]
        public InputField inputUser;


        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Load()
        {
            base.Load();
        }

        public override void Unload()
        {
            if (unloadUserInput)
                inputUser.text = string.Empty;

            base.Unload();
        }

        public void Write(string message)
        {
            textOutput.text += $"\n {message}";
        }

        public void Wipe()
        {
            textOutput.text = string.Empty;
        }

        public void ScrollToEnd()
        {
            scrollerVertical.value = 0f;
        }

        public void Focus()
        {
            inputUser.Select();
        }
    }
}
