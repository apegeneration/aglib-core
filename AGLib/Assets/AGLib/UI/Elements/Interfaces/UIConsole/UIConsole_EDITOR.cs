﻿using System.Collections;
using System.Collections.Generic;
using AGLib.UI.Views;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;


public class UIConsole_EDITOR : MonoBehaviour
{
#if (UNITY_EDITOR)

    /// <summary>
    /// Add object to scene
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("GameObject/AGLib/UI/Interfaces/UIConsole", false, 0)]
    private static void AddObject(MenuCommand menuCommand)
    {
        // Create object
        var go = new GameObject("UIInterface: Console");
        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
        go.AddComponent<RectTransform>().sizeDelta = new Vector2(400f, 160f);

        // Set up scroll view
        var scrollview = new GameObject("ScrollView: Console View");
        GameObjectUtility.SetParentAndAlign(scrollview, go);
        var sr = scrollview.AddComponent<ScrollRect>();
        sr.horizontal = false;
        sr.movementType = ScrollRect.MovementType.Clamped;
        sr.inertia = false;
        sr.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.Permanent;

        var srt = scrollview.GetComponent<RectTransform>();
        srt.anchorMin = new Vector2(0f, 0f);
        srt.anchorMax = new Vector2(1f, 1f);
        srt.sizeDelta = new Vector2(0f, 0f);

        // Set up user input field
        var inputfield = new GameObject("Input: User input");
        GameObjectUtility.SetParentAndAlign(inputfield, go);
        var ir = inputfield.AddComponent<RectTransform>();
        var i = inputfield.AddComponent<InputField>();
        i.transition = Selectable.Transition.None;

        // Set input field RectTransform values
        ir.position = new Vector2(0f, -13f);
        ir.anchorMin = new Vector2(0f, 0f);
        ir.anchorMax = new Vector2(1f, 0f);
        ir.sizeDelta = new Vector2(0f, 30f);

        // Set up input field text element
        var t = new GameObject("Text: User input");
        GameObjectUtility.SetParentAndAlign(t, inputfield);
        var txt = t.AddComponent<Text>();
        txt.fontSize = 14;
        txt.color = Color.green;
        txt.supportRichText = false;

        // Add text component
        i.textComponent = txt;

        // Set text element rect
        var tr = t.GetComponent<RectTransform>();

        tr.anchorMin = new Vector2(0f, 0f);
        tr.anchorMax = new Vector2(1f, 1f);

        tr.sizeDelta = new Vector2(0f, 0f);

        // Register the creation in the Unity undo system
        Undo.RegisterCreatedObjectUndo(go, "Created " + go.name);

        // Select object
        Selection.activeObject = go;
    }

#endif
}