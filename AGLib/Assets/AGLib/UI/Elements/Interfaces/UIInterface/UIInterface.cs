﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGLib.UI.Interfaces
{
    public class UIInterface : MonoBehaviour
    {
        // Identifier string
        [Tooltip("Interface identifier string")]
        public string identifier;


        /// <summary>
        /// On initialize
        /// </summary>
        public virtual void Initialize()
        {
        }

        /// <summary>
        /// On interface load
        /// </summary>
        public virtual void Load()
        {
        }

        /// <summary>
        /// On interface unload
        /// </summary>
        public virtual void Unload()
        {
        }
    }
}
