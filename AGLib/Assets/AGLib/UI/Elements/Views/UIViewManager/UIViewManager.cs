﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AGLib.UI.Views
{
    public class UIViewManager : MonoBehaviour
    {
        [Tooltip("Debug to Unity console & log")]
        public bool debugToUnity;

        [Tooltip("Keeps object this class is on alive through scenes")]
        public bool keepAlive;

        [Tooltip("UIView to start with")]
        public string viewStart;

        [Tooltip("UIView current")]
        public string viewCurrent;

        private List<UIView> cachedViews;


        private void Awake()
        {
            if (keepAlive)
                DontDestroyOnLoad(gameObject);
        }

        public void Initialize()
        {
            // Initial debug message
            if (debugToUnity)
                Debug.Log("[UIViewManager] Initializing manager.");

            // Find views in hierarchy
            RefreshViewCache(true);
        }

        public void RefreshViewCache(bool initializing = false)
        {
            if (debugToUnity)
                Debug.Log("[UIViewManager] Refreshing view cache.");

            cachedViews = FindObjectsOfType<UIView>().ToList();

            for (var i = 0; i < cachedViews.Count; i++)
            {
                // Initialize view
                cachedViews[i].Initialize();

                // Activate start view
                var start = cachedViews[i].identifier.Equals(viewStart);
                cachedViews[i].gameObject.SetActive(start);

                if (start)
                    cachedViews[i].Load();
            }

            viewCurrent = viewStart;

            if (debugToUnity)
                Debug.Log("[UIViewManager] Completed view cache refresh.");
        }

        public void ViewLoad(string identifier, bool unload = true)
        {
            // Unload current view
            if (unload)
                ViewUnload();

            var index = FindIndex(identifier);
            if (index != -1)
            {
                cachedViews[index].gameObject.SetActive(true);
                cachedViews[index].Load();

                if (debugToUnity)
                    Debug.Log($"[UIViewManager] View: {identifier} loaded.");
            }
            else
            {
                if (debugToUnity)
                    Debug.LogWarning($"[UIViewManager] Unable to load view: {identifier}.");
            }
        }

        public void ViewUnload()
        {
            ViewUnload(viewCurrent);
        }

        public void ViewUnload(string identifier)
        {
            var index = FindIndex(identifier);

            if (index != -1)
            {
                cachedViews[index].Unload();
                cachedViews[index].gameObject.SetActive(false);

                if (debugToUnity)
                    Debug.Log($"[UIViewManager] View: {identifier} unloaded.");
            }
            else
            {
                if (debugToUnity)
                    Debug.LogWarning($"[UIViewManager] Unable to unload view: {identifier}.");
            }
        }

        public void ViewDispose(string identifier)
        {
            var index = FindIndex(identifier);
            if (index != -1)
            {
                Destroy(cachedViews[index].gameObject);

                if (debugToUnity)
                    Debug.Log($"[UIViewManager] View: {identifier} disposed.");
            }
            else
            {
                if (debugToUnity)
                    Debug.LogWarning($"[UIViewManager] Unable to dispose view: {identifier}.");
            }
        }

        public UIView ViewGet(string identifier)
        {
            var index = FindIndex(identifier);
            return index == -1 ? null : cachedViews[index];
        }

        private int FindIndex(string identifier)
        {
            return cachedViews.FindIndex(x => x.identifier.Equals(identifier));
        }
    }
}