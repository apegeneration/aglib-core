﻿using System.Collections;
using System.Collections.Generic;
using AGLib.UI;
using AGLib.UI.Views;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIViewManager_EDITOR : MonoBehaviour
{
#if (UNITY_EDITOR)

    /// <summary>
    /// Add object to scene
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("GameObject/AGLib/UI/UIViewManager", false, 0)]
    private static void AddObject(MenuCommand menuCommand)
    {
        // Create object
        var go = new GameObject("UI: View Manager");
        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
        go.AddComponent<UIViewManager>();
        var canvas = go.AddComponent<Canvas>();
        var scaler = go.AddComponent<CanvasScaler>();
        go.AddComponent<GraphicRaycaster>();

        // Set up canvas
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.pixelPerfect = true;
        canvas.sortingOrder = 2;

        // Set up scaler
        scaler.scaleFactor = 1;
        scaler.referencePixelsPerUnit = 100;

        // Set up Event Manager
        var em = new GameObject("Event Manager");
        GameObjectUtility.SetParentAndAlign(em, go);
        em.AddComponent<EventSystem>();
        em.AddComponent<StandaloneInputModule>();

        // Register the creation in the Unity undo system
        Undo.RegisterCreatedObjectUndo(go, "Created " + go.name);

        // Select object
        Selection.activeObject = go;
    }

#endif
}
