﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGLib.UI.Views
{
    /// <summary>
    /// UIView
    /// 
    /// UIView is a view control base class for AGLib UI.
    /// Refer to documentation for more information.
    /// </summary>
    public class UIView : MonoBehaviour
    {
        // Identifier
        [Tooltip("View identifier string")]
        public string identifier;

        // Fade speed
        [Tooltip("View fade speed (speed * 20 = time in seconds)")]
        public float fadeSpeed = 0.05f;

        // Fade amount
        private const float FADE_AMOUNT = 1f / 20f;

        // Attached canvas group
        private CanvasGroup canvasGroup;

        // Calculated fade time
        private WaitForSeconds fadeTime = new WaitForSeconds(0.05f);

        /// <summary>
        /// Initialize view
        /// </summary>
        public virtual void Initialize()
        {
            if (canvasGroup == null)
                canvasGroup = GetComponent<CanvasGroup>() 
                    ?? gameObject.AddComponent<CanvasGroup>();

            FadeSet(fadeSpeed);
        }

        /// <summary>
        /// Load view
        /// </summary>
        public virtual void Load()
        {
        }

        /// <summary>
        /// Unload view
        /// </summary>
        public virtual void Unload()
        {
        }

        /// <summary>
        /// Fade out view
        /// </summary>
        public virtual void FadeOut()
        {
            StartCoroutine(Fade(false));
        }

        /// <summary>
        /// Fade in view
        /// </summary>
        public virtual void FadeIn()
        {
            StartCoroutine(Fade(true));
        }

        /// <summary>
        /// Fade method
        /// </summary>
        /// <param name="fadein">Fade in true</param>
        /// <returns>N/A</returns>
        public IEnumerator Fade(bool fadein)
        {
            for (var t = 0; t < 20; t++)
            {
                yield return fadeTime;

                canvasGroup.alpha += fadein ? FADE_AMOUNT : -FADE_AMOUNT;
            }

            if (fadein && canvasGroup.alpha < 1f)
                canvasGroup.alpha = 1f;

            if (!fadein && canvasGroup.alpha > 0f)
                canvasGroup.alpha = 0f;
        }

        /// <summary>
        /// Set fade speed
        /// </summary>
        /// <param name="speed">Speed</param>
        public void FadeSet(float speed)
        {
            fadeTime = new WaitForSeconds(speed);
        }
    }
}
