﻿using System.Collections;
using System.Collections.Generic;
using AGLib.UI.Views;
using UnityEditor;
using UnityEngine;


/// <summary>
/// UIView_EDITOR
/// 
/// Description:
/// UIView editor class.
/// </summary>
public class UIView_EDITOR : MonoBehaviour
{
#if (UNITY_EDITOR)

    /// <summary>
    /// Add object to scene
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("GameObject/AGLib/UI/UIView", false, 0)]
    private static void AddObject(MenuCommand menuCommand)
    {
        // Check if parent is canvas
        var p = menuCommand.context as GameObject;
        if (p != null && p.GetComponent<Canvas>() != null)
        {
            // Create object
            var go = new GameObject("View: N/A");
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            var rt = go.AddComponent<RectTransform>();
            go.AddComponent<CanvasRenderer>();
            go.AddComponent<UIView>();
            go.AddComponent<CanvasGroup>();

            // Set up rect transform dimensions & anchoring
            rt.anchorMin = new Vector2(0f, 0f);
            rt.anchorMax = new Vector2(1f, 1f);

            rt.sizeDelta = new Vector2(0f, 0f);

            // Register the creation in the Unity undo system
            Undo.RegisterCreatedObjectUndo(go, "Created " + go.name);

            // Select object
            Selection.activeObject = go;
        }
        else
            Debug.LogError("[EDITOR] Can't add UIView to an object that doesn't have a Canvas.");
    }

#endif
}
