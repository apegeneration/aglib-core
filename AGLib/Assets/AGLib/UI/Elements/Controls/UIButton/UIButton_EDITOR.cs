﻿using AGLib.UI;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIButton_EDITOR : MonoBehaviour
{
#if (UNITY_EDITOR)

    /// <summary>
    /// Add object to scene
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("GameObject/AGLib/UI/UIButton", false, 0)]
    private static void AddObject(MenuCommand menuCommand)
    {
        // Create objects
        var go = new GameObject("Button: ");
        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

        var txt = new GameObject("Text: Button");
        GameObjectUtility.SetParentAndAlign(txt, go);
        
        // Add components
        go.AddComponent<Image>();
        var button = go.AddComponent<UIButton>();
        var t = txt.AddComponent<Text>();

        // Set size
        go.GetComponent<RectTransform>().sizeDelta = new Vector2(160f, 30f);

        // Set transition colors
        var cb = button.colors;
        cb.normalColor = Color.white;
        cb.highlightedColor = new Color(0.9f, 0.9f, 0.9f);
        cb.pressedColor = Color.white;
        cb.disabledColor = new Color(0.5f, 0.5f, 0.5f);
        button.colors = cb;

        // Set navigation
        var nav = button.navigation;
        nav.mode = Navigation.Mode.None;
        button.navigation = nav;

        // Set up text
        t.fontSize = 15;
        t.supportRichText = false;
        t.color = Color.black;
        t.alignment = TextAnchor.MiddleCenter;
        t.text = "Button";

        // Set up text rect transform dimensions & anchoring
        var rt = txt.GetComponent<RectTransform>();
        rt.anchorMin = new Vector2(0f, 0f);
        rt.anchorMax = new Vector2(1f, 1f);

        rt.sizeDelta = new Vector2(-12f, -6f);

        // Register the creation in the Unity undo system
        Undo.RegisterCreatedObjectUndo(go, "Created " + go.name);

        // Select object
        Selection.activeObject = go;
    }

#endif
}
