﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AGLib.UI
{
    public class UIButton : Button
    {
        [Tooltip("Sprite cache for fast switching")]
        public Sprite[] cachedSprites = new Sprite[2];


        public void SetCachedSprite(int index)
        {
            if (cachedSprites.Length <= index || index < 0)
                return;

            GetComponent<Image>().sprite = cachedSprites[index];
        }
    }
}