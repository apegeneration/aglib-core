﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIInputField_EDITOR : MonoBehaviour
{
#if (UNITY_EDITOR)

    private static readonly Color COLOR_IMAGE = new Color(0.05f, 0.05f, 0.05f);
    private static readonly Color COLOR_TEXT = new Color(0.95f, 0.95f, 0.95f);


    /// <summary>
    /// Add object to scene
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("GameObject/AGLib/UI/UIInputField", false, 0)]
    private static void AddObject(MenuCommand menuCommand)
    {
        // Create object
        var go = new GameObject("InputField: ");
        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

        // Generate
        var image = go.AddComponent<Image>();
        image.color = COLOR_IMAGE;
        var inputfield = go.AddComponent<InputField>();
        var txt = new GameObject("Text: Input");
        var t = txt.AddComponent<Text>();

        GameObjectUtility.SetParentAndAlign(txt, go);

        // Change dimensions
        inputfield.GetComponent<RectTransform>().sizeDelta = new Vector2(160f, 28f);

        // Disable transition
        inputfield.transition = Selectable.Transition.None;

        // Disable navigation
        var nav = inputfield.navigation;
        nav.mode = Navigation.Mode.None;
        inputfield.navigation = nav;
        inputfield.textComponent = t;

        // Set up text
        t.fontSize = 15;
        t.supportRichText = false;
        t.color = COLOR_TEXT;
        t.alignment = TextAnchor.MiddleLeft;

        // Set up text rect transform dimensions & anchoring
        var rt = txt.GetComponent<RectTransform>();
        rt.anchorMin = new Vector2(0f, 0f);
        rt.anchorMax = new Vector2(1f, 1f);

        rt.sizeDelta = new Vector2(-12f, -6f);

        // Register the creation in the Unity undo system
        Undo.RegisterCreatedObjectUndo(go, "Created " + go.name);

        // Select object
        Selection.activeObject = go;
    }

#endif
}
