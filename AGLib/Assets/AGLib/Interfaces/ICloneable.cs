﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGLib.Interfaces
{
    /// <summary>
    /// Interface to make a class clone-able
    /// </summary>
    public interface ICloneable<out T>
    {
        T Clone();
    }
}