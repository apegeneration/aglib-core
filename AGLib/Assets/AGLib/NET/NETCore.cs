﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace AGLib.NET
{
    public class NETCore : MonoBehaviour
    {
        // Channels
        public static int CHANNEL_RELIABLE;
        public static int CHANNEL_UNRELIABLE;

        public static NETDecoder decoder;

        // Debug
        [Tooltip("Debug to Unity console")]
        public bool debugToUnity;

        // Socket
        [Tooltip("Socket (auto)")]
        public int socket;

        // Connection port
        [Tooltip("Connection port")]
        public int port;

        // Maximum amount of connections
        [Tooltip("Maximum amount of connections")]
        public int connectionMax;

        // Connection ID
        public int connectionId;

        // Initialization status
        public bool isInitialized;


        /// <summary>
        /// Initialize network core
        /// </summary>
        /// <param name="server">Network entity is server?</param>
        public void Setup(bool server)
        {
            // Initialize network transport
            NetworkTransport.Init();

            // Set up connection configuration
            var config = new ConnectionConfig();

            // Set up channels
            CHANNEL_RELIABLE = config.AddChannel(QosType.Reliable);
            CHANNEL_UNRELIABLE = config.AddChannel(QosType.Unreliable);

            // Set up connection
            if (connectionMax == 0)
                connectionMax = 2;

            var c = new HostTopology(config, connectionMax);

            // Initialize socket
            socket = server
                ? NetworkTransport.AddHost(c, port)
                :NetworkTransport.AddHost(c);

            isInitialized = true;
        }
    }
}
