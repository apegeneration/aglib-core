﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace AGLib.NET
{
    [AddComponentMenu("Ape Generation/NET/NETServer")]
    public class NETServer : NETCore
    {
        public static NETServer instance;
        public static NETServerEncoder encoder;

        // Connections
        public static Dictionary<int, string> activeConnections;


        #if UNITY_EDITOR
        private void Start()
        {
            Initialize();
        }
        #endif

        public void Initialize()
        {
            instance = this;

            // Encoder
            encoder = new NETServerEncoder(this);

            // Decoder (Replace with custom decoder)
            decoder = new NETDecoder();

            activeConnections = new Dictionary<int, string>();

            // Initialize core
            Setup(true);
        }

        public void SetDecoder(NETDecoder d)
        {
            decoder = d;
        }

        private void Update()
        {
            NetworkEventType networkEvent;
            do
            {
                int hostId;
                int sender;
                int channelId;
                var buffer = new byte[NETPacket.SIZE];
                int dataSize;
                byte error;

                networkEvent = NetworkTransport.Receive(out hostId, out sender,
                    out channelId, buffer, NETPacket.SIZE, out dataSize, out error);

                switch (networkEvent)
                {
                    case NetworkEventType.Nothing:
                        continue;

                    case NetworkEventType.ConnectEvent:
                        activeConnections.Add(sender, string.Empty);

                        if (debugToUnity)
                            Debug.Log("[NETServer] Remote client connected. " +
                                      $"ID: {sender} | Total: {activeConnections.Count}");
                        continue;

                    case NetworkEventType.DataEvent:
                        decoder.Decode(sender, buffer);
                        continue;

                    case NetworkEventType.DisconnectEvent:
                        activeConnections.Remove(sender);

                        if (debugToUnity)
                            Debug.Log("[NETServer] Remote client disconnected. " +
                                      $"ID: {sender} | Total: {activeConnections.Count}");
                        continue;
                    case NetworkEventType.BroadcastEvent:
                        continue;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            } while (networkEvent != NetworkEventType.Nothing);
        }

        public void SendPacket(byte[] packet, int target)
        {
            SendPacket(packet, target, CHANNEL_RELIABLE);
        }

        public void SendPacket(byte[] packet, int target, int channel)
        {
            if (!activeConnections.ContainsKey(target))
            {
                if (debugToUnity)
                    Debug.Log($"[NETServer] Unable to send packet to ID: {target}, no such connection.");
                return;
            }

            byte error;
            NetworkTransport.Send(0, target, channel,
                packet, NETPacket.SIZE, out error);

            if (debugToUnity)
                Debug.Log($"[NETJob_SendPacket] ({error}) Sending packet to ID: {target}.");
        }

        public void SendPacketAll(byte[] packet)
        {
            SendPacketAll(packet, CHANNEL_RELIABLE);
        }

        public void SendPacketAll(byte[] packet, int channel)
        {
            var keys = activeConnections.Keys.GetEnumerator();
            var k = keys.Current;
            for (var i = 0; i < activeConnections.Count; i++)
            {
                k = keys.Current;

                SendPacket(packet, k, channel);

                keys.MoveNext();
            }

            // Dispose keys enumerator
            keys.Dispose();
        }

        public void SendPacketAll(byte[] packet, int channel, int exclude)
        {
            var keys = activeConnections.Keys.GetEnumerator();
            var k = keys.Current;
            for (var i = 0; i < activeConnections.Count; i++)
            {
                k = keys.Current;

                if (k != exclude)
                    SendPacket(packet, k, channel);

                keys.MoveNext();
            }

            // Dispose keys enumerator
            keys.Dispose();
        }
    }
}
