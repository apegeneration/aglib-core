﻿using System;
using System.Collections;
using System.Collections.Generic;
using MessagePack;
using UnityEngine;

namespace AGLib.NET
{
    public class NETServerEncoder
    {
        private readonly NETServer core;


        public NETServerEncoder(NETServer core)
        {
            this.core = core;
        }

        public void SendPacket(byte id, byte op, int target, bool reliable = true)
        {
            // Create packet
            var packet = new NETPacket(id, op);

            // Send packet
            Send(packet, target, reliable);
        }

        public void SendPacket<T>(byte id, byte op, T data, int target, bool reliable = true)
        {
            // Create packet
            var packet = new NETPacket(id, op);
            packet.Write(data);

            // Send packet
            Send(packet, target, reliable);
        }

        private void Send(NETPacket packet, int target, bool reliable = true)
        {
            // Null checks
            if (core == null)
                return;

            if (packet == null)
            {
                if (core.debugToUnity)
                    Debug.LogWarning(
                        "[NETClientEncoder] Trying to send null packet, this should never happen!");
                return;
            }

            if (reliable)
                core.SendPacket(Encode(packet), target);
            else
                core.SendPacket(Encode(packet), target, NETCore.CHANNEL_UNRELIABLE);
        }

        public void SendPacketAll(byte id, byte op, bool reliable = true)
        {
            // Create packet
            var packet = new NETPacket(id, op);

            // Send packet
            SendAll(packet, reliable);
        }

        public void SendPacketAll(byte id, byte op, int exclude, bool reliable = true)
        {
            // Create packet
            var packet = new NETPacket(id, op);

            // Send packet
            SendAll(packet, exclude, reliable);
        }

        public void SendPacketAll<T>(byte id, byte op, T data, bool reliable = true)
        {
            // Create packet
            var packet = new NETPacket(id, op);
            packet.Write(data);

            // Send packet
            SendAll(packet, reliable);
        }

        public void SendPacketAll<T>(byte id, byte op, T data, int exclude, bool reliable = true)
        {
            // Create packet
            var packet = new NETPacket(id, op);
            packet.Write(data);

            // Send packet
            SendAll(packet, exclude, reliable);
        }

        private void SendAll(NETPacket packet, bool reliable = true)
        {
            // Null checks
            if (core == null)
                return;

            if (packet == null)
            {
                if (core.debugToUnity)
                    Debug.LogWarning(
                        "[NETClientEncoder] Trying to send null packet, this should never happen!");
                return;
            }

            if (reliable)
                core.SendPacketAll(Encode(packet));
            else
                core.SendPacketAll(Encode(packet), NETCore.CHANNEL_UNRELIABLE);
        }

        private void SendAll(NETPacket packet, int exclude, bool reliable = true)
        {
            // Null checks
            if (core == null)
                return;

            if (packet == null)
            {
                if (core.debugToUnity)
                    Debug.LogWarning(
                        "[NETClientEncoder] Trying to send null packet, this should never happen!");
                return;
            }

            core.SendPacketAll(Encode(packet), 
                reliable ? NETCore.CHANNEL_RELIABLE : NETCore.CHANNEL_UNRELIABLE, 
                exclude);
        }

        private static byte[] Encode(NETPacket packet)
        {
            return MessagePackSerializer.Serialize(packet);
        }
    }
}