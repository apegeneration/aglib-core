﻿using MessagePack;
using UnityEngine;

namespace AGLib.NET
{
    public class NETClientEncoder
    {
        private readonly NETClient core;


        public NETClientEncoder(NETClient core)
        {
            this.core = core;
        }

        public void SendPacket(byte id, byte op)
        {
            // Create packet
            var packet = new NETPacket(id, op);

            // Send packet
            Send(packet);
        }

        public void SendPacket<T>(byte id, byte op, T data)
        {
            // Create packet
            var packet = new NETPacket(id, op);
            packet.Write(data);

            // Send packet
            Send(packet);
        }

        private void Send(NETPacket packet)
        {
            // Null checks
            if (core == null)
                return;

            if (packet == null)
            {
                if (core.debugToUnity)
                    Debug.LogWarning(
                        "[NETClientEncoder] Trying to send null packet, this should never happen!");
                return;
            }

            core.SendPacket(Encode(packet));
        }

        private static byte[] Encode(NETPacket packet)
        {
            return MessagePackSerializer.Serialize(packet);
        }
    }
}