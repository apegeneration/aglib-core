﻿using System;
using System.Linq;
using System.Text;
using MessagePack;

namespace AGLib.NET
{
    [Serializable]
    [MessagePackObject(true)]
    public class NETPacket
    {
        // Size constant
        public const int SIZE = 508;

        // ID code
        public byte id;

        // OP code
        public byte op;

        // Payload
        public byte[] data;


        public NETPacket() { }

        public NETPacket(int id, int op)
        {
            this.id = (byte) id;
            this.op = (byte) op;
        }

        public NETPacket(byte id, byte op)
        {
            this.id = id;
            this.op = op;
        }

        public NETPacket(int id, int op, byte[] data)
        {
            this.id = (byte) id;
            this.op = (byte) op;
            this.data = data;
        }

        public NETPacket(byte id, byte op, byte[] data)
        {
            this.id = id;
            this.op = op;
            this.data = data;
        }

        public void Write<T>(T target)
        {
            data = MessagePackSerializer.Serialize(target);
        }

        public object Read()
        {
            return data == null ? null 
                : MessagePackSerializer.Deserialize<object>(data);
        }
    }
}
