﻿using MessagePack;
using UnityEngine;

namespace AGLib.NET
{
    /// <summary>
    /// NETDecoder
    /// 
    /// Description:
    /// Use this as base class to decode raw packets into NETPackets.
    /// The handle method is made to be overridden and handle the different
    /// IDCode / OPCode combinations. 
    /// </summary>
    public class NETDecoder
    {
        /// <summary>
        /// Decodes raw packet into NETPacket
        /// </summary>
        /// <param name="sender">Sender integer ID</param>
        /// <param name="rawpacket">Raw incoming packet</param>
        public void Decode(int sender, byte[] rawpacket)
        {
            var packet = 
                MessagePackSerializer.Deserialize<NETPacket>(rawpacket);

            if (packet == null) return;

            Handle(sender, packet);
        }

        /// <summary>
        /// Handles the decoded packet
        /// </summary>
        /// <param name="sender">Sender integer ID</param>
        /// <param name="packet">Decoded packet</param>
        public virtual void Handle(int sender, NETPacket packet)
        {
            Debug.Log($"Decoded packet: {packet.id} : {packet.op}");
        }
    }
}
