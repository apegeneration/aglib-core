﻿using System.Collections;
using System.Collections.Generic;
using AGLib.NET;
using UnityEditor;
using UnityEngine;

public class NETClient_EDITOR : MonoBehaviour
{
#if (UNITY_EDITOR)

    /// <summary>
    /// Add object to scene
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("GameObject/AGLib/NET/NETClient", false, 0)]
    private static void AddObject(MenuCommand menuCommand)
    {
        // Create object
        var go = new GameObject("NETClient");
        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

        // Add network component
        go.AddComponent<NETClient>();

        // Register the creation in the Unity undo system
        Undo.RegisterCreatedObjectUndo(go, "Created " + go.name);

        // Select object
        Selection.activeObject = go;
    }

#endif
}
