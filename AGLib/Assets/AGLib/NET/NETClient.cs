﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Networking;

namespace AGLib.NET
{
    [AddComponentMenu("Ape Generation/NET/NETClient")]
    public class NETClient : NETCore
    {
        [Tooltip("Target IP address")]
        public string address = "127.0.0.1";

        // Connection status
        public static bool isConnected;

        public static NETClient instance;
        public static NETClientEncoder encoder;


        private void Start()
        {
            Initialize(true);
        }

        public void Initialize(bool connect)
        {
            instance = this;

            // Encoder
            encoder = new NETClientEncoder(this);

            // Decoder
            decoder = new NETDecoder();

            // Set up connection
            Setup(false);

            // Connect
            if (connect)
                Connect();
        }

        public void SetDecoder(NETDecoder d)
        {
            decoder = d;
        }

        public void Connect()
        {
            byte error;
            connectionId = NetworkTransport.Connect(0, address, port, 0, out error);

            if (debugToUnity && error > 0)
                Debug.Log($"[NETClient] ({error}) Error attempting to connect to {address}:{port}.");
        }

        public void Update()
        {
            int hostId;
            int sender;
            int channelId;
            var buffer = new byte[NETPacket.SIZE];
            int dataSize;
            byte error;

            var networkEvent = NetworkTransport.Receive(out hostId, out sender,
                out channelId, buffer, NETPacket.SIZE, out dataSize, out error);

            switch (networkEvent)
            {
                case NetworkEventType.Nothing:
                    break;

                case NetworkEventType.ConnectEvent:
                    isConnected = true;

                    if (debugToUnity)
                        Debug.Log("[NETClient] Connected to host.");

                    break;

                case NetworkEventType.DataEvent:
                    decoder.Decode(sender, buffer);
                    break;

                case NetworkEventType.DisconnectEvent:
                    isConnected = false;

                    if (debugToUnity)
                        Debug.Log("[NETClient] Disconnected from host.");
                    break;
            }
        }

        public void SendPacket(byte[] packet)
        {
            SendPacket(packet, CHANNEL_RELIABLE);
        }

        public void SendPacket(byte[] packet, int channel)
        {
            byte error;
            NetworkTransport.Send(0, connectionId, channel,
                packet, NETPacket.SIZE, out error);
        }
    }
}