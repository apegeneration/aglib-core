﻿using UnityEngine;
using UnityEngine.UI;

namespace AGLib.NET
{
    public class ApeNetExample_Button_Ping : MonoBehaviour
    {
        public Button button;


        // Use this for initialization
        void Start()
        {
            button.onClick.AddListener(() =>
            {
                for (var i = 0; i < 500; i++)
                {
                    NETClient.encoder.SendPacket(IDCode.SYSTEM, OPCode.PING);
                }
            });
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}