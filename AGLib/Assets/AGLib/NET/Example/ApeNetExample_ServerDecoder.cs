﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGLib.NET
{
    public class ApeNetExample_ServerDecoder : NETDecoder
    {
        public override void Handle(int sender, NETPacket packet)
        {
            base.Handle(sender, packet);

            switch (packet.id)
            {
                case IDCode.SYSTEM:
                    switch (packet.op)
                    {
                        case OPCode.PING:
                            break;
                    }
                    break;
            }
        }
    }
}
