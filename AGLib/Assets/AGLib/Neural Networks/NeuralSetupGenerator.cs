﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGLib.NeuralNetworks
{
    public static class NeuralSetupGenerator
    {
        public static int[] GenerateFeedForward(int input)
        {

            var setup = new[] {input, input, input-1};

            return setup;
        }

        public static int[] GenerateFeedForward(int input, int output)
        {
            var setup = new[] {input, input, output};

            return setup;
        }

        public static int[] GenerateDeepFeedForward(int input)
        {
            var setup = new[] {input, input + 1, input + 1, input - 1};

            return setup;
        }

        //public static int[] GenerateDeepFeedForward(int input, int)
    }
}