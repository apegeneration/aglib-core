﻿using System;
using System.Collections;
using MessagePack;
using Unity.Collections;
using Unity.Jobs;

namespace AGLib.NeuralNetworks
{
    [Serializable]
    [MessagePackObject(true)]
    public class BPNet
    {
        public static float learnRate = 0.03f;

        public int[] layer;
        public BPLayer[] layers;


        public BPNet(int[] layer)
        {
            this.layer = new int[layer.Length];
            for (var i = 0; i < layer.Length; i++)
                this.layer[i] = layer[i];

            layers = new BPLayer[layer.Length - 1];

            for (var i = 0; i < layers.Length; i++)
                layers[i] = new BPLayer(layer[i], layer[i + 1]);
        }

        public float[] FeedForward(float[] inputs)
        {
            layers[0].FeedForward(inputs);

            for (var i = 1; i < layers.Length; i++)
                layers[i].FeedForward(layers[i - 1].outputs);

            return layers[layers.Length - 1].outputs;
        }

        /*
        public float[] FeedForwardWithJobs(float[] inputs)
        {
            
        }
        */

        public void BackPropagation(float[] expected)
        {
            for (var i = layers.Length - 1; i >= 0; i--)
            {
                if (i == layers.Length - 1)
                    layers[i].BackPropagationOutput(expected);
                else
                    layers[i].BackPropagationHidden(layers[i + 1].gamma, layers[i + 1].weights);
            }

            for (var i = 0; i < layers.Length; i++)
                layers[i].UpdateWeights();
        }
    }
}