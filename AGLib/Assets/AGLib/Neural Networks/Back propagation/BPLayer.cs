﻿using System;
using MessagePack;

namespace AGLib.NeuralNetworks
{
    [Serializable]
    [MessagePackObject(true)]
    public class BPLayer
    {
        public int inputCount;
        public int outputCount;

        public float[] outputs;
        public float[] inputs;
        public float[,] weights;
        public float[,] weightsDelta;
        public float[] gamma;
        public float[] error;

        private readonly Random rnd = new Random();


        public BPLayer(int inputCount, int outputCount)
        {
            this.inputCount = inputCount;
            this.outputCount = outputCount;

            inputs = new float[inputCount];
            outputs = new float[outputCount];
            weights = new float[outputCount, inputCount];
            weightsDelta = new float[outputCount, inputCount];
            gamma = new float[outputCount];
            error = new float[outputCount];

            InitWeights();
        }

        public void InitWeights()
        {
            for (var i = 0; i < outputCount; i++)
            for (var j = 0; j < inputCount; j++)
                weights[i, j] = (float) rnd.NextDouble() - 0.5f;
        }

        public void UpdateWeights()
        {
            for (var i = 0; i < outputCount; i++)
                for (var j = 0; j < inputCount; j++)
                    weights[i, j] -= weightsDelta[i, j] * BPNet.learnRate;
        }

        public float[] FeedForward(float[] inputs)
        {
            this.inputs = inputs;

            for (var i = 0; i < outputCount; i++)
            {
                outputs[i] = 0;
                for (var k = 0; k < inputCount; k++)
                    outputs[i] += inputs[k] * weights[i, k];

                outputs[i] = (float) Math.Tanh(outputs[i]);
            }

            return outputs;
        }

        public void BackPropagationOutput(float[] expected)
        {
            for (var i = 0; i < outputCount; i++)
                error[i] = outputs[i] - expected[i];

            for (var i = 0; i < outputCount; i++)
                gamma[i] = error[i] * TanHDerivative(outputs[i]);

            for (var i = 0; i < outputCount; i++)
                for (var j = 0; j < inputCount; j++)
                    weightsDelta[i, j] = gamma[i] * inputs[j];
        }

        public void BackPropagationHidden(float[] gammaForward, float[,] weightsForward)
        {
            for (var i = 0; i < outputCount; i++)
            {
                gamma[i] = 0;

                for (var j = 0; j < gammaForward.Length; j++)
                    gamma[i] += gammaForward[j] * weightsForward[j, i];

                gamma[i] *= TanHDerivative(outputs[i]);
            }

            for (var i = 0; i < outputCount; i++)
                for (var j = 0; j < inputCount; j++)
                    weightsDelta[i, j] = gamma[i] * inputs[j];
        }

        public float TanHDerivative(float value)
        {
            return 1 - value * value;
        }
    }
}