﻿using System;
using System.Collections.Generic;
using MessagePack;

namespace AGLib.NeuralNetworks
{
    /// <summary>
    /// MTNet
    /// 
    /// Description:
    /// Mutation based neural network.
    /// </summary>
    [Serializable]
    [MessagePackObject(true)]
    public class MTNet
    {
        // Neural network fitness
        public float fitness;

        // Layers
        public int[] layers;

        // Neurons
        public float[][] neurons;

        // Weights
        public float[][][] weights;


        /// <summary>
        /// MTNet
        /// </summary>
        /// <param name="layers">Layer setup</param>
        public MTNet(int[] layers)
        {
            this.layers = new int[layers.Length];

            for (var i = 0; i < layers.Length; i++)
                this.layers[i] = layers[i];

            InitializeNeurons();
            InitializeWeights();
        }

        /// <summary>
        /// MTNet
        /// </summary>
        /// <param name="network">Existing MTNet</param>
        public MTNet(MTNet network)
        {
            layers = new int[network.layers.Length];

            for (var i = 0; i < layers.Length; i++)
                layers[i] = network.layers[i];

            InitializeNeurons();
            InitializeWeights();
            CopyWeights(network.weights);
        }

        /// <summary>
        /// MTNet
        /// </summary>
        /// <param name="parent1">Parent 1</param>
        /// <param name="parent2">Parent 2</param>
        /// <param name="random">Random mutation after birth</param>
        public MTNet(MTNet parent1, MTNet parent2, bool random = false)
        {
            layers = new int[parent1.layers.Length];

            for (var i = 0; i < layers.Length; i++)
                layers[i] = parent1.layers[i];

            InitializeNeurons();
            InitializeWeights();
            MutateFromParents(parent1, parent2, random);
        }

        /// <summary>
        /// MTNet
        /// </summary>
        /// <param name="network">Layer setup</param>
        /// <param name="fit">Copy fitness value</param>
        public MTNet(MTNet network, bool fit)
        {
            if (fit)
                fitness = network.fitness;

            layers = new int[network.layers.Length];

            for (var i = 0; i < layers.Length; i++)
                layers[i] = network.layers[i];

            InitializeNeurons();
            InitializeWeights();
            CopyWeights(network.weights);
        }

        /// <summary>
        /// Create neuron matrix
        /// </summary>
        private void InitializeNeurons()
        {
            var neuronsList = new List<float[]>();

            // Add layers to neuron list
            for (var i = 0; i < layers.Length; i++)
                neuronsList.Add(new float[layers[i]]);

            neurons = neuronsList.ToArray();
        }

        /// <summary>
        /// Create weights matrix.
        /// </summary>
        private void InitializeWeights()
        {
            // Weight list
            var weightsList = new List<float[][]>();

            for (var i = 1; i < layers.Length; i++)
            {
                var layerWeightsList = new List<float[]>();

                var neuronsInPreviousLayer = layers[i - 1];

                for (var j = 0; j < neurons[i].Length; j++)
                {
                    var neuronWeights = new float[neuronsInPreviousLayer];

                    // Set weights randomly between 0.5f and -0.5
                    for (var k = 0; k < neuronsInPreviousLayer; k++)
                        neuronWeights[k] = UnityEngine.Random.Range(-0.5f, 0.5f);

                    layerWeightsList.Add(neuronWeights); //add neuron weights of this current layer to layer weights
                }

                // Add weights to list
                weightsList.Add(layerWeightsList.ToArray());
            }

            // Convert to 3D array
            weights = weightsList.ToArray();
        }

        /// <summary>
        /// Feed forward this neural network with a given input array
        /// </summary>
        /// <param name="inputs">Inputs to network</param>
        /// <returns></returns>
        public float[] FeedForward(float[] inputs)
        {
            // Add to neuron matrix
            for (var i = 0; i < inputs.Length; i++)
                neurons[0][i] = inputs[i];

            // Compute values
            for (var i = 1; i < layers.Length; i++)
            {
                for (var j = 0; j < neurons[i].Length; j++)
                {
                    var value = 0f;

                    // Sum up weight connections
                    for (var k = 0; k < neurons[i - 1].Length; k++)
                        value += weights[i - 1][j][k] * neurons[i - 1][k];
                    
                    // Hyperbolic tangent activation
                    neurons[i][j] = (float)Math.Tanh(value);
                }
            }

            return neurons[neurons.Length - 1];
        }

        /// <summary>
        /// Mutate neural network weights randomly
        /// </summary>
        public void MutateRandom()
        {
            for (var i = 0; i < weights.Length; i++)
            {
                for (var j = 0; j < weights[i].Length; j++)
                {
                    for (var k = 0; k < weights[i][j].Length; k++)
                    {
                        var weight = weights[i][j][k];

                        //mutate weight value 
                        var rnd = UnityEngine.Random.Range(0f, 8f);

                        if (rnd <= 2f)
                        {
                            // Flip sign of weight
                            weight *= -1f;
                        }
                        else if (rnd <= 4f)
                        { 
                            // Pick random weight between -1 and 1
                            weight = UnityEngine.Random.Range(-0.5f, 0.5f);
                        }
                        else if (rnd <= 6f)
                        {
                            // Randomly increase by 0% - 100%
                            var factor = UnityEngine.Random.Range(0f, 1f) + 1f;
                            weight *= factor;
                        }
                        else if (rnd <= 8f)
                        { 
                            // Randomly decrease by 0% - 100%
                            var factor = UnityEngine.Random.Range(0f, 1f);
                            weight *= factor;
                        }

                        weights[i][j][k] = weight;
                    }
                }
            }
        }

        /// <summary>
        /// Mutate neural network with parents
        /// </summary>
        public void MutateFromParents(MTNet parent1, MTNet parent2, bool random = false)
        {
            for (var i = 0; i < neurons.Length; i++)
            {
                for (var j = 0; j < neurons[i].Length; j++)
                {
                    var p1 = UnityEngine.Random.Range(0, 2) == 0;
                    neurons[i][j] = p1 ? parent1.neurons[i][j] : parent2.neurons[i][j];
                }
            }

            for (var i = 0; i < weights.Length; i++)
            {
                for (var j = 0; j < weights[i].Length; j++)
                {
                    for (var k = 0; k < weights[i][j].Length; k++)
                    {
                        weights[i][j][k] = UnityEngine.Random.Range(0, 2) == 0
                            ? parent1.weights[i][j][k]
                            : parent2.weights[i][j][k];
                    }
                }
            }

            if (random)
                MutateRandom();
        }

        /// <summary>
        /// Compare two neural networks
        /// </summary>
        /// <param name="other">Network to be compared to</param>
        public int CompareTo(MTNet other)
        {
            if (other == null) return 1;

            return fitness < other.fitness ? -1 : 1;
        }

        /// <summary>
        /// Copy Weights
        /// </summary>
        /// <param name="weights">Weights to copy</param>
        private void CopyWeights(float[][][] weights)
        {
            for (var i = 0; i < this.weights.Length; i++)
                for (var j = 0; j < this.weights[i].Length; j++)
                    for (var k = 0; k < this.weights[i][j].Length; k++)
                        this.weights[i][j][k] = weights[i][j][k];
        }

    }
}