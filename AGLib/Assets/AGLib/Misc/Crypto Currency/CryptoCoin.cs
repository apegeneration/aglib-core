﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGLib.Misc.CryptoCurrency
{
    [Serializable]
    public class CryptoCoin
    {
        // Short name
        public string nameShort;

        // Long / Normal name
        public string nameLong;

        // Symbol eg. BTC
        public string nameSymbol;

        // Company / Owner name
        public string nameCompany;

        // Today's data
        public CryptoHistory dataNow;

        // Data history
        public List<CryptoHistory> dataHistory = new List<CryptoHistory>();


        public CryptoCoin()
        {
        }

        public CryptoCoin(string nameShort, string nameLong,
            string nameSymbol, string nameCompany)
        {
            this.nameShort = nameShort;
            this.nameLong = nameLong;
            this.nameSymbol = nameSymbol;
            this.nameCompany = nameCompany;
        }
        public CryptoCoin(string nameShort, string nameLong,
            string nameSymbol, string nameCompany, CryptoHistory dataNow)
        {
            this.nameShort = nameShort;
            this.nameLong = nameLong;
            this.nameSymbol = nameSymbol;
            this.nameCompany = nameCompany;

            this.dataNow = dataNow;
        }
    }
}
