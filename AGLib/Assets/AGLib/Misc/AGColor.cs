﻿using System;
using MessagePack;
using UnityEngine;
using Color = UnityEngine.Color;

namespace AGLib.Misc
{
    [Serializable]
    [MessagePackObject(true)]
    public class AGColor
    {
        // Identifier
        public string name;

        // Hex code
        public string hex;

        // Color
        public Color color;


        public AGColor()
        {
        }

        public AGColor(string hex)
        {
            this.hex = hex;
            color = HexToColor(hex);
        }

        public AGColor(Color color)
        {
            this.color = color;
            hex = ColorToHex(color);
        }

        public string GetInfo()
        {
            return $"Name: {name} | Hex: {hex} | Color: {GetRGBA()}";
        }

        public string GetRGBA()
        {
            return $"{color.r}.{color.g}.{color.b} a:{color.a}";
        }

        public static string ColorToHex(Color color)
        {
            var hex = $"{color.r:X2}{color.r:X2}{color.b:X2}{color.a:X2}";

            return hex;
        }

        public static Color HexToColor(string hex)
        {
            return HexToColor(hex, 255);
        }

        public static Color HexToColor(string hex, byte alpha)
        {
            var r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            var g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            var b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

            return new Color32(r, g, b, alpha);
        }
    }
}