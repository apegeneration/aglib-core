# Ape Generation Library

Ape Generation Library (AGLib) is a collection of classes, tools & editor extensions for Unity.  
The extension aims to improve and add to Unity's existing functionality.  

**Major features**

+ Neural Networks
	* _Neural networks and neural network related functionality._
+ Unity UI Extension
	* _Full UI system built using core Unity UI parts._
+ Networking
	* _Full network system built on Unity's LLAPI._
+ Game dev
	* _Tools and game development systems._


**Minor features**

+ Name Generator
	* _Generate names (extendible by custom name packs)._
+ MessagePack
	* _MessagePack is a lightning fast serialization library!_
	* _Integrated in every major system in AGLib but often optional to use._