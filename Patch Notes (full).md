# Ape Generation Library patch notes (full list)  

* * *

**v1.0.3**

+ **Editor**
	* Modified several editor menu item names.

+ **Misc**
	* AGColor class (specialized color class).

+ **NET**
	* IDCode sample class.
	* OPCode sample class.
	* Optimized NETServer to try and handle all queued packets per frame.

+ **UI**
	* UIViewManager class.
	* Renamed UIView Init method to Initialize.
	* UIInputField editor class.
	* Simplified UI namespaces.
	* Removed several redundant class references.
	* UIButton class (Unity UI button extension).
	* UIButton editor class.


* * *

**v1.0.2**

+ **3rd Party**
	* Added the MessagePack license to MessagePack folder.

+ **Content**
	* Inventory (full version) WIP.

+ **Interfaces**
	* ICloneable - Interface for class deep copy.

+ **NET**
	* NETServerEncoder class (base class for handling outgoing packets).
	* Changed packet structure to a more simple and generic approach.
	* Updated NETClientEncoder to conform to new packet structure.

* * *

**v1.0.1**

+ **NET**
	* Editor class for NETServer.
	* Added NETServer to component menu (Ape Generation/NET/ApeNET/Server).
	* Editor class for NETClient.
	* Added NETClient to component menu (Ape Generation/NET/ApeNET/Client).
	* NETDecoder class (base class for handling incoming packets).
	* NETClientEncoder class (base class for handling outgoing packets).

+ **UI**
	* Editor class for UIView.
	* Editor class for UIViewManager

* * *